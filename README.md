# Ubuntu Based h20.jar Dockerfile

## nginx as http forward

`nginx` is used to port forward `HTTP` traffix from `8080` to `54321` with, as relevant part of `nginx.conf`;

```
        server {
                listen 8080;
                location / {
                        proxy_pass http://127.0.0.1:54321;
                }
        }
```

## Example `build` command

build with for instance

```
docker build .
```