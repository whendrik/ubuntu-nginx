FROM ubuntu

EXPOSE 8080

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y curl unzip
RUN apt-get install -y nginx
RUN apt-get clean

ADD nginx.conf /etc/nginx/nginx.conf
RUN mkdir /root/www
ADD index.html /root/www/index.html

RUN rm /etc/nginx/sites-enabled/default
RUN cd && mkdir nginx

ADD start.sh /root/start.sh
RUN chmod +x /root/start.sh
WORKDIR /root

CMD ["./start.sh"]